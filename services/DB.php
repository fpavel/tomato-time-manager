<?php

class DB
{

	private $pdo;

	public function __construct(PDO $pdo) {
		$this->pdo = $pdo;
	}

	public function fetchAll($sql, $params = []) {
		$sth = $this->pdo->prepare($sql);
		$sth->execute($params);
		$result = [];
		if (!$sth->rowCount()) return $result;
		foreach ($sth->fetchAll(PDO::FETCH_ASSOC) as $row) {
			$result[] = $row;
		}
		return $result;
	}

	public function fetchAssoc($sql, $params = []) {
		$result = $this->fetchAll($sql, $params);
		return count($result) ? $result[0] : null;
	}

	public function execute($sql, $params = []) {
		$sth = $this->pdo->prepare($sql);
		$sth->execute($params);
	}

	public function fetchField($sql, $params = []) {
		$sth = $this->pdo->prepare($sql);
		$sth->execute($params);
		return $sth->fetchColumn();
	}

	public function lastInsertId() {
		return $this->pdo->lastInsertId();
	}

	public function insert($table, $fields) {
		$columns = array_keys($fields);
		$placeholders = array_map(function($field) {
			return ':' . $field;
		}, $columns);
		$sql = 'INSERT INTO ' . $table . ' (' . implode(',', $columns) . ') values (' . implode(',', $placeholders) . ')';
		$sth = $this->pdo->prepare($sql);
		$sth->execute($fields);
		return $this->lastInsertId();
	}
}