<?php

class UserService
{
	private $db;
	private $cookieName;

	public function __construct(DB $db) {
		$this->db = $db;
		$this->cookieName = 'tomato-time-manager-session-' . md5($_SERVER['REMOTE_ADDR']);
	}

	public function authorize($user, $pass): User {
		$username = strtolower(trim(filter_var($user, FILTER_SANITIZE_STRING)));
		$password = md5($pass);
		$email = strtolower(trim(filter_var($user, FILTER_SANITIZE_EMAIL)));
		$data = $this->db->fetchAssoc(
			'SELECT * FROM users WHERE (username=? OR email=?) AND password=? LIMIT 1',
			[$username, $email, $password]
		);
		$user = new User($data);
		if ($user->id) $this->setUserSession($user);
		return $user;
	}

	public function createUser($request) {
		if ($request['password'] !== $request['password2']) return false;
		$username = strtolower(trim(filter_var($request['username'], FILTER_SANITIZE_STRING)));
		$email = strtolower(trim(filter_var($request['email'], FILTER_SANITIZE_EMAIL)));
		$password = md5($request['password']);
		$apikey = $session = md5(microtime() . $email);
		$user = $this->db->fetchAssoc(
			'SELECT * FROM users WHERE username=? OR email=? LIMIT 1',
			[$username, $email]
		);
		if ($user) return false;
		$this->db->insert('users', ['username' => $username, 'email' => $email, 'password' => $password, 'apikey' => $apikey]);
		return (bool)$this->db->lastInsertId();
	}

	public function logout() {
		$sesion = $this->getUserSession();
		$user = $this->getUserFromSession();
		if (!empty($sesion) && $user) {
			$this->db->execute('DELETE from session_to_user WHERE user=? AND session=?', [$user->id, $sesion]);
		}
		unset($_COOKIE[$this->cookieName]);
		setcookie($this->cookieName, null, -1);
		app()->redirect('/login.php');
	}


	public function getUserFromSession(): User {
		$session = $this->getUserSession();
		if (empty($session)) return new User();
		$user = $this->db->fetchAssoc(
			'SELECT users.* FROM users LEFT JOIN session_to_user s2u ON s2u.user = users.id WHERE s2u.session=? LIMIT 1',
			[filter_var($session, FILTER_SANITIZE_STRING)]
		);
		return new User($user);
	}

	private function setUserSession(User $user) {
		$session = md5(microtime() . $_SERVER['REMOTE_ADDR']);
		$this->db->insert('session_to_user', ['session' => $session, 'user' => $user->id]);
		setcookie($this->cookieName, $session,time() + (86400 * 30)); // 30 days expire
	}
	private function getUserSession() {
		return empty($_COOKIE[$this->cookieName]) ? '' : $_COOKIE[$this->cookieName];
	}

	public function getUserFromApikey($apikey): User {
		$user = $this->db->fetchAssoc(
			'SELECT * FROM users WHERE apikey=? LIMIT 1',
			[trim(filter_var($apikey, FILTER_SANITIZE_STRING))]
		);
		return new User($user);
	}

	public function getAllUsers() {
		$users = $this->db->fetchAll('SELECT * from users');
		return array_map(function($user) {
			return new User($user);
		}, $users);
	}

}