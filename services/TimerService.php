<?php

class TimerService
{
	private $db;

	public function __construct(DB $db) {
		$this->db = $db;
	}

	public function getActive($userId)
	{
		$sql = '
			SELECT
				timers.*, (timers.start + types.length - ' . time() . ') as timeleft, types.name, types.length
			FROM
				timers
			LEFT JOIN
				types ON timers.type = types.id
			WHERE
				end is null
				AND user = ' . (int)$userId . '
			ORDER BY
				start DESC
		';
		return array_map([$this, 'makeTimer'], $this->db->fetchAll($sql));
	}

	public function updateComment($userId, $timerId, $comment = '') {
		$this->db->execute('UPDATE timers SET comment=? WHERE id=? AND user=? LIMIT 1', [$comment, (int)$timerId, (int)$userId]);
	}

	public function updateLogged($userId, $timerId, $logged = true)
	{
		$this->db->execute('UPDATE timers SET logged=? WHERE id=? AND user=? LIMIT 1', [(int)$logged, (int)$timerId, (int)$userId]);
	}

	public function getAll()
	{
		$sql = '
			SELECT
				timers.*
			FROM
				timers
			LEFT JOIN
				types ON timers.type = types.id
			ORDER BY
				start ASC
		';
		return array_map([$this, 'makeTimer'], $this->db->fetchAll($sql));
	}

	public function stop($userId, $timerId, $completed = true)
	{
		$sql = 'UPDATE timers SET end = '.time().' , completed = '.(int)$completed.' WHERE id = '.(int)$timerId.' AND user = '.(int)$userId.' LIMIT 1';
		$this->db->execute($sql);
		return $this->getByIdAndUser($userId, $timerId);
	}

	public function start($userId, $type)
	{
		$this->db->insert('timers', ['user' => (int)$userId, 'start' => time(), 'type' => (int) $type]);
		return $this->db->lastInsertId();
	}

	public function getById($id)
	{
		return $this->makeTimer($this->db->fetchAssoc('SELECT * FROM timers WHERE id=? LIMIT 1', [(int)$id]));
	}

	public function getByIdAndUser($userId, $timerId)
	{
		return $this->makeTimer($this->db->fetchAssoc('SELECT * FROM timers WHERE user=? AND id=? LIMIT 1', [(int)$userId, (int)$timerId]));
	}

	public function getNextType($userId)
	{
		$lastTimer = $this->makeTimer($this->db->fetchAssoc('SELECT * FROM timers WHERE user='.(int)$userId.' ORDER BY start DESC LIMIT 1'));
		// if last time was a break, then next is pomodoro
		if ($lastTimer->type > 1) {
			return 1;
		}
		if ($lastTimer->type=== 1 && $lastTimer->completed === 0) {
			return 1;
		}
		$lastLongStart = $this->db->fetchField('SELECT start FROM timers WHERE type = 3 ORDER BY start DESC LIMIT 1');
		if (empty($lastLongStart)) {
			$lastLongStart = 0;
		}
		$prevPomodoros = $this->db->fetchField('SELECT count(*) FROM timers WHERE completed = 1 AND type = 1 AND start > ?', [(int)$lastLongStart]);
		if ($prevPomodoros < 4) {
			return 2; // short break
		}
		return 3; // long break
	}

	public function stopAll($userId)
	{
		$active = $this->getActive($userId);
		foreach ($active as $timer) {
			// always mark breaks as completed
			if ($timer->type > 1) {
				$this->stop($timer->id, true);
			} else {
				$this->stop($timer->id, $timer->timeleft < 1);
			}
		}
	}

	public function stopCompleted($userId)
	{
		$active = $this->getActive($userId);
		foreach ($active as $timer) {
			if ($timer->timeleft < 1) {
				$this->stop($userId, $timer->id);
			}
		}
	}

	public function getCompleted($user)
	{
		$sql = '
			SELECT
				timers.*
			FROM
				timers
			LEFT JOIN
				types ON timers.type = types.id
			WHERE
				timers.completed = 1
				AND user = ' . (int)$user . '
			ORDER BY
				start DESC
		';
		return array_map([$this, 'makeTimer'], $this->db->fetchAll($sql));
	}


	public function getCompletedByDay($userId) {
		$timers = $this->getCompleted($userId);
		$timersByDay = [];
		foreach ($timers as $timer) {
			if ($timer->type !== 1) continue;
			$day = date("Y-m-d l", $timer->start);
			if (!isset($timersByDay[$day])) $timersByDay[$day] = ['day' => $day, 'timers' => []];
			$timersByDay[$day]['timers'][] = $timer;
		}
		return $timersByDay;
	}

	public function getLastWeekCompleted($user, int $offset = 0, int $count = 4) {
		$week = $count * $offset + $count;
		$weekDate = new DateTime();
		$weekDate->modify('-' . $week . ' weeks');

		$dateFrom = clone $weekDate;
		$dateFrom->modify('monday');
		$dateTo = clone $weekDate;
		$dateTo->modify('+' . $count .' weeks');

		return $this->getCompletedByTime($user, $dateFrom->getTimestamp(), $dateTo->getTimestamp());

		/*$sql = '
			SELECT
				timers.*
			FROM
				timers
			LEFT JOIN
				types ON timers.type = types.id
			WHERE
				timers.completed = 1
				AND type = 1
				AND start > ' . $dateFrom->getTimestamp() . '
				AND start <= ' . $dateTo->getTimestamp() . '
				AND user = ' . (int)$user . '
			ORDER BY
				start DESC
		';
		return array_map([$this, 'makeTimer'], $this->db->fetchAll($sql));*/
	}

	public function getLastWeekCompletedByDay(int $user, int $week = 0) {
		$d = new DateTime('today');
		$today = $d->getTimestamp();
		$yesterday = $today - 86400;
		$timersByDay = [];
		$timers = $this->getLastWeekCompleted($user);
		foreach ($timers as $timer) {
			$day = date("l, F jS", $timer->start);
			if ($timer->start > $today) {
				$dayHuman = 'Today';
			} elseif ($timer->start > $yesterday) {
				$dayHuman = 'Yesterday';
			} else {
				$dayHuman = $day;
			}
			if (!isset($timersByDay[$day])) $timersByDay[$day] = ['day' => $dayHuman, 'date' => $day, 'timers' => []];
			$timersByDay[$day]['timers'][] = $timer;
		}
		return array_values($timersByDay);
	}

	public function getCompletedByTime(int $user, int $timeStart, int $timeEnd) {
		if (empty($timeEnd)) $timeEnd = time() + 86400; // today
		if (empty($timeStart)) $timeStart = time() - 4 * 7 * 24 * 60 * 60; // 4 weaks ago
		$sql = '
			SELECT
				timers.*
			FROM
				timers
			LEFT JOIN
				types ON timers.type = types.id
			WHERE
				timers.completed = 1
				AND type = 1
				AND start > ' . $timeStart . '
				AND start <= ' . $timeEnd . '
				AND user = ' . $user . '
			ORDER BY
				start DESC
		';
		return array_map([$this, 'makeTimer'], $this->db->fetchAll($sql));
	}

	private function makeTimer($timer): Timer
	{
		return new Timer($timer);
	}
}
