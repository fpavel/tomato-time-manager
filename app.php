<?php
date_default_timezone_set('Europe/Moscow');

ini_set('display_errors', 1);
error_reporting(-1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

require __DIR__.'/models/Timer.php';
require __DIR__.'/models/User.php';
require __DIR__.'/services/DB.php';
require __DIR__.'/services/TimerService.php';
require __DIR__.'/services/UserService.php';
require __DIR__.'/api/TimersApi.php';

class App {

	public $db;
	public $timerService;
	private $path = __DIR__;

	private function __construct() {
		$this->db = new DB(new PDO('mysql:dbname=pomodoro;host=127.0.0.1','pomodoro', '123'));
		$this->timerService = new TimerService($this->db);
		$this->userService = new UserService($this->db);
	}

	private function __clone() {}

	// singleton
	public static function instance()
	{
		static $inst = null;
		if ($inst === null) {
			$inst = new self();
		}
		return $inst;
	}

	public function render($viewName, $data = []) {
		$file = $this->path . '/views/' . $viewName . '.php';
		if (!file_exists($file)) return;
		if (!empty($data) && is_array($data)) extract($data, EXTR_OVERWRITE);
		include $file;
	}

	public function renderModule($moduleName) {
		$file = $this->path . '/modules/' . $moduleName . '.php';
		if (file_exists($file)) include $file;
	}

	public function redirect($url) {
		header('Location: ' . $url);
		die();
	}
}

function gol($message) {
	$debug = debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS);
	$some_text = $debug[1]['function'] .': ['. $debug[0]['line'] . '] ' . $debug[0]['file'];
	if( is_array( $message ) || is_object( $message ) ){
		error_log( $some_text . "\n" . print_r( $message, true )  . "\n\n" );
	} else {
		error_log( $some_text . "\n" . $message . "\n\n" );
	}
}

// instantiate application
App::instance();

// alias for app instance
function app(): App {
	return App::instance();
}

