<?php

class User
{
	public $id;
	public $username;
	public $email;
	public $apikey;
	public $status;
	public $active;

	public function __construct($data = [])
	{
		$this->id = !empty($data['id']) ? (int)$data['id'] : null;
		$this->username = !empty($data['username']) ? $data['username'] : null;
		$this->email = !empty($data['email']) ? $data['email'] : null;
		$this->apikey = !empty($data['apikey']) ? $data['apikey'] : null;
		$this->status = !empty($data['status']) ? $data['status'] : null;
		$this->active = !empty($data['status']) ? (int)$data['active'] : 0;
	}

	public function getGravatarUrl($size = 40) {
		return 'https://www.gravatar.com/avatar/' . md5(strtolower(trim($this->email))) . '?s=' . $size;
	}
}
