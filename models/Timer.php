<?php

class Timer implements JsonSerializable
{
	public $id;
	public $user;
	public $type;
	public $name;
	public $length;
	public $start;
	public $end;
	public $timeleft;
	public $completed;
	public $logged;
	public $comment;

	public function __construct($data)
	{
		$this->id = !empty($data['id']) ? (int)$data['id'] : null;
		$this->user = !empty($data['user']) ? (int)$data['user'] : null;
		$this->type = !empty($data['type']) ? (int)$data['type'] : null;
		$this->name = !empty($data['name']) ? $data['name'] : null;
		$this->length = !empty($data['length']) ? (int)$data['length'] : null;
		$this->start = !empty($data['start']) ? (int)$data['start'] : null;
		$this->end = !empty($data['end']) ? (int)$data['end'] : null;
		$this->timeleft = !empty($data['timeleft']) ? (int)$data['timeleft'] : null;
		$this->completed = (int)$data['completed'];
		$this->logged = (int)$data['logged'];
		$this->comment = !empty($data['comment']) ? $data['comment'] : '';
	}

    private function formatEnd() {
        if ($this->end) return date('H:i', $this->end);
        if ($this->start && $this->length) return date('H:i', $this->start + $this->length);
        return '';
    }

	public function jsonSerialize()
	{
		return [
			'id' => $this->id,
			'user' => $this->user,
			'type' => $this->type,
			'name' => $this->name,
			'start' => $this->start,
			'end' => $this->end,
			'length' => $this->length,
			'timeleft' => $this->timeleft,
			'completed' => $this->completed,
			'logged' => $this->logged,
			'comment' => $this->comment,
			'formatStart' => $this->start ? date('H:i', $this->start) : '',
			'formatEnd' => $this->formatEnd(),
		];
	}
}
