// http://eslint.org/docs/user-guide/configuring
module.exports = {
  root: true,
  parser: 'babel-eslint',
  parserOptions: {
    sourceType: 'module'
  },
  env: {
    browser: true,
    node: true,
  },
  plugins: [
    'react'
  ],
  extends: ['eslint:recommended', 'plugin:react/recommended'],
  globals: {
    'React': true,
    'Pomodoro': true,
  },
  // check if imports actually resolve
  'settings': {
    'import/resolver': {
      'webpack': {
        'config': 'webpack.config.js'
      }
    }
  },
  // add your custom rules here
  'rules': {
    'prefer-const': 1,
    'indent': [
      'off',
      2,
      {'SwitchCase': 1}
    ],
    'linebreak-style': [
      'error',
      'unix'
    ],
    'quotes': [
      'warn',
      'single'
    ],
    'semi': [
      'error',
      'always'
    ],
    'no-useless-escape': 0,
    'no-console': 'off',
    // require traling comma in multiline arrays/object so that we have less merge conflicts
    // https://eslint.org/docs/rules/comma-dangle
    'comma-dangle': ['off', {
      'arrays': 'always-multiline',
      'objects': 'always-multiline',
      'imports': 'never',
      'exports': 'never',
      'functions': 'ignore'
    }],
    'no-unused-vars': ['error', { vars: 'all', args: 'none', varsIgnorePattern: '_$|^React$' }],
    'no-undef': 2,
    'react/jsx-uses-vars': 2,
    'react/prop-types': ['off'],
  }
}


