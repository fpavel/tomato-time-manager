<div class="login">
	<form action="/register.php" method="POST">
		<h1>Register</h1>
		<label>
			Username
			<input type="text" name="username" required>
		</label>
		<label>
			Email
			<input type="email" name="email" required>
		</label>
		<label>
			Password
			<input type="password" name="password" required>
		</label>
		<label>
			Password Confirm
			<input type="password" name="password2" required>
		</label>
		<div class="text-center"><button>Sign up</button></div>
		<p class="text-center"><a href="/login.php">Log in</a></p>
	</form>
</div>