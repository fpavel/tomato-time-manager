<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport"
				content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<link rel="icon" href="/images/tomato.png">
	<link rel="stylesheet" href="/style.css">
	<script src="/script.js"></script>
	<title>Tomato time manager</title>
	<script type="text/javascript">
		const apikey = '<?php echo $apikey ?? ''; ?>';
	</script>
</head>
<body>