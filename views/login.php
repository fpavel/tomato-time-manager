<div class="login">
	<form action="/login.php" method="POST">
		<h1>Login</h1>
		<label>
			Username or email
			<input type="text" name="user">
		</label>
		<label>
			Password
			<input type="password" name="password">
		</label>
		<div class="text-center"><button>Login</button></div>
		<p class="text-center"><a href="/register.php">Sign up here</a></p>
	</form>
</div>