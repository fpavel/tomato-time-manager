<div class="all-timers container">
	<table>
		<tr>
			<th>day</th>
			<th class="tomatos-count">tomatoes</th>
			<th>time</th>
			<th>comment</th>
			<th>logged</th>
		</tr>
		<?php foreach ($days as $day): ?>
			<?php $timersCount = count($day['timers']) ?>
			<tr>
				<td rowspan="<?php echo $timersCount + 1 ?>"><?php echo $day['day'] ?></td>
				<td class="tomatos-count" rowspan="<?php echo $timersCount + 1 ?>"><?php echo $timersCount ?></td>
			</tr>
			<?php foreach ($day['timers'] as $timer): ?>
				<tr>
					<td><?php echo date('H:i', $timer->start) ?> - <?php echo date('H:i', $timer->end) ?></td>
					<td>
						<form data-timer="<?php echo $timer->id ?>" onsubmit="Pomodoro.updateComment(this);return false;">
							<input class="comment" onchange="Pomodoro.updateComment(this)" placeholder="Comment" class="comment" type="text" value="<?php echo $timer->comment ?>">
						</form>
					</td>
					<td>
						<?php if ($timer->logged): ?>
							<input onchange="Pomodoro.updateLogged(this)" data-timer="<?php echo $timer->id ?>" type="checkbox" checked="checked">
						<?php else: ?>
							<input onchange="Pomodoro.updateLogged(this)" data-timer="<?php echo $timer->id ?>" type="checkbox">
						<?php endif; ?>
					</td>
				</tr>
			<?php endforeach ?>
		<?php endforeach ?>
	</table>
</div>
