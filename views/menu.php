<header class="menu">
	<div class="container">
		<div class="clearfix">
			<div class="menu-logo">
				<a href="index.php">Tomato time manager</a>
			</div>
			<div class="menu-container">
				<ul>
					<li><a href="/index.php">Home</a></li>
					<li><a href="/users.php">Users</a></li>
					<?php if ($user->id): ?>
						<li><a href="/report.php">Report</a></li>
						<li><a href="/profile.php">Profile</a></li>
						<li><a href="/logout.php">Log out</a></li>
					<?php else: ?>
						<li><a href="/login.php">Login</a></li>
						<li><a href="/register.php">Sing up</a></li>
					<?php endif; ?>
				</ul>
			</div>
		</div>
	</div>
</header>