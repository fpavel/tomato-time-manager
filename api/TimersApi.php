<?php
namespace api;


class TimersApi
{
	private $path;
	private $method;
	private $data;
	private $user;
	private $timersService;

	public function __construct()	{
		$this->method = $_SERVER['REQUEST_METHOD'];
		$this->path = rtrim($_SERVER['PATH_INFO'], '/');
		foreach (explode(';', $_SERVER['QUERY_STRING']) as $param) {
			$keyValue = explode('=', $param);
			if (count($keyValue) < 2 || $keyValue[0] !== 'apikey') continue;
			$this->user = app()->userService->getUserFromApikey($keyValue[1]);
		}
		$this->timersService = app()->timerService;
		if ($this->method === 'PUT' || $this->method === 'POST') {
			$this->data = json_decode(file_get_contents("php://input"), true);
		}
	}

	public function processRequest() {
		if (!$this->user->id) {
			$this->errorResponse();
			return;
		}
		if ($this->method === 'POST' && $this->path === '/timers/start/pomodoro') {
			$this->startPomodoro();
			return;
		}
		if ($this->method === 'POST' && $this->path === '/timers/start/short') {
			$this->startShort();
			return;
		}
		if ($this->method === 'POST' && $this->path === '/timers/start/long') {
			$this->startLong();
			return;
		}
		if ($this->method === 'POST' && $this->path === '/timers/start') {
			$this->startAuto();
			return;
		}
		if ($this->method === 'GET' && $this->path === '/timers/completed') {
			$this->getCompleted();
			return;
		}
		if ($this->method === 'GET' && $this->path === '/timers/all') {
			$this->getAll();
			return;
		}
		if ($this->method === 'GET' && $this->path === '/timers/recent') {
			$this->getRecent();
			return;
		}
		if ($this->method === 'PUT' && preg_match('/\/timers\/(\d+)/', $this->path, $matches)) {
			$this->updateTimer((int)$matches[1], $this->data);
			return;
		}
		if ($this->method === 'GET' && $this->path === '/timers') {
			$this->getActive();
			return;
		}
		$this->errorResponse('wrong method');
	}

	/*
	 * Use for getting current timer info
	 * Should be called from some loop
	 * If timeleft is 0 or less, we complete this timer.
	 * After that alert should be called
	 */
	private function getActive() {
		$active = $this->timersService->getActive($this->user->id);
		$this->timersService->stopCompleted($this->user->id);
		$length = count($active);
		if ($length === 0) {
			$this->jsonResponse([
				'start' => null,
				'type' => null,
				'timeleft' => null,
				'next' => $this->timersService->getNextType($this->user->id)
			]);
		} else {
			$this->jsonResponse($active[$length - 1]);
		}
	}

	/**
	 * Retrieves all timers from database.
	 * Can be slow.
	 */
	private function getAll() {
		$active = $this->timersService->getAll();
		$this->jsonResponse($active);
	}

	/**
	 * Retrieves only completed timers from database.
	 */
	private function getCompleted() {
		$active = $this->timersService->getCompleted($this->user->id);
		$this->jsonResponse(array_map([$this, 'formatTimer'], $active));
	}

	private function startAuto()
	{
		$this->start($this->timersService->getNextType($this->user->id));
	}

	private function start($type)
	{
		$id = $this->user->id;
		$this->timersService->stopAll($id);
		$this->timersService->start($id, $type);
		$this->jsonResponse(['status' => 'success']);
	}

	private function startPomodoro()
	{
		$this->start(1);
	}

	private function startShort()
	{
		$this->start(2);
	}

	private function startLong()
	{
		$this->start(3);
	}

	private function updateTimer($timerId, $data)
	{
		$userId = $this->user->id;
		if (isset($data['comment'])) {
			$this->timersService->updateComment($userId, $timerId, $data['comment']);
		}
		if (isset($data['logged'])) {
			$this->timersService->updateLogged($userId, $timerId, $data['logged']);
		}
		$this->jsonResponse($this->timersService->getByIdAndUser($userId, $timerId));
	}

	private function getRecent()
	{
//		$this->jsonResponse($this->timersService->getLastWeekCompletedByDay($this->user->id));
		$dateStart = $_GET['dateStart'] ?? 0;
		$dateEnd = $_GET['dateEnd'] ?? 0;
		$this->jsonResponse($this->timersService->getCompletedByTime($this->user->id, $dateStart, $dateEnd));
	}

	private function errorResponse($message = '')
	{
		http_response_code(400);
		die(json_encode(['status' => 'error', 'message' => $message]));
	}

	private function jsonResponse($data = [])
	{
		header('Content-Type: application/json');
		header('Access-Control-Allow-Origin: *');
		http_response_code(200);
		die(json_encode($data));
	}


}