import { PureComponent } from 'react';
import DaysContainer from '../containers/DaysContainer';
import RefreshButton from './RefreshButton';
import Countdown from './Countdown';
import Day from './Day';

class Root extends PureComponent {
  constructor() {
    super();
    this.updateData = this.updateData.bind(this);
    this.handleFilterChange = this.handleFilterChange.bind(this);
  }
  updateData() {
    this.props.fetchTimers();
    this.props.fetchActiveTimer();
  }
  componentDidMount() {
    this.updateData();
  }
  handleFilterChange(event) {
    this.props.updateFilter(event.target.value);
  }
  render() {
    const { fetchTimers, activeTimer, fetchActiveTimer, filter } = this.props;
    const activeTomato = activeTimer.type === 1
      ? <Day key="current-timer" day="Current tomato" timers={[activeTimer]} />
      : null;
    return (
      <div className="root-container">
        <div className="root-container-header">
          <RefreshButton onClick={this.updateData} />
          <input onChange={this.handleFilterChange} type="text" placeholder="Filter" value={filter} />
          <Countdown onStop={this.updateData} onClick={fetchActiveTimer} activeTimer={activeTimer} />
        </div>
        <div className="recent-timers">
          <div className="current-tomato">{activeTomato}</div>
          <DaysContainer fetchTimers={fetchTimers} />
        </div>
      </div>
    );
  }
}

export default Root;
