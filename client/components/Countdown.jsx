import { Component } from 'react';

class Countdown extends Component {
  constructor(props) {
    super(props);
    this.state = {timeleft: ''};
    this.handleClick = this.handleClick.bind(this);
  }

  start() {
    if (this.countdownInterval) clearInterval(this.countdownInterval);
    this.countdownInterval = setInterval(() => { this.runCountdown(this.props.activeTimer); }, 500);
  }

  stop(triggerStop) {
    if (!this.countdownInterval) return;
    clearInterval(this.countdownInterval);
    this.setState({ timeleft: '' });
    if (triggerStop) this.props.onStop(); // update timers on timer end
  }

  runCountdown({ name, start, length }) {
    const secondsLeft = (start + length) - (Date.now() / 1000 >> 0);
    if (isNaN(secondsLeft) || secondsLeft < 1) {
      this.stop(true);
      return;
    }
    const minutes = secondsLeft / 60 >> 0;
    const seconds = secondsLeft - minutes * 60;
    this.setState({
      timeleft: name + ': ' + (minutes < 10 ? '0' + minutes : minutes) + ':' + (seconds < 10 ? '0' + seconds : seconds),
    });
  }

  handleClick(e) {
    e.preventDefault();
    this.props.onClick();
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.activeTimer.start === this.props.activeTimer.start) return;
    nextProps.activeTimer.start && nextProps.activeTimer.length ? this.start() : this.stop();
  }

  shouldComponentUpdate(nextProps, nextState) {
    return nextState.timeleft !== this.state.timeleft;
  }

  componentWillUnmount() {
    this.stop();
  }

  render() {
    const timeleft = this.state.timeleft;
    return <a href="#" onClick={this.handleClick} className={'current-timer ' + (timeleft ? 'active' : '')}>{ timeleft || 'No timer is running' }</a>;
  }
}

export default Countdown;
