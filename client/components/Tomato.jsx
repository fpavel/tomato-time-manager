import { PureComponent } from 'react';
import debounce from 'lodash/debounce';
import { timerPut } from '../actions/timers-actions';

class Tomato extends PureComponent {
  constructor(props) {
    super(props);
    this.handleCommentChange = this.handleCommentChange.bind(this);
    this.handleLoggedChange = this.handleLoggedChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.updateTimer = this.updateTimer.bind(this);
    this.delayUpdateTimer = debounce(this.updateTimer, 2000);
    this.state = {comment: props.comment, logged: props.logged};
  }
  componentWillReceiveProps(props) {
    this.setState({comment: props.comment, logged: props.logged});
  }
  handleCommentChange(event) {
    this.setState({comment: event.target.value}, this.delayUpdateTimer);
  }
  handleLoggedChange(event) {
    this.setState({logged: event.target.checked}, this.updateTimer);
  }
  updateTimer() {
    const { id, dispatch } = this.props;
    const { comment, logged } = this.state;
    if (comment === this.props.comment && logged === this.props.logged) return;
    dispatch(timerPut({ id, comment, logged }));
  }
  handleSubmit(event) {
    event.preventDefault();
    this.updateTimer();
  }
  render() {
    const { formatStart, formatEnd } = this.props;
    const { comment, logged } = this.state;
    return (
      <form onSubmit={this.handleSubmit} className="timer-wrapper">
        <div className="timer-time">{formatStart} - {formatEnd}</div>
        <div className="timer-comment-wrapper">
          <input
            name="timerComment"
            type="text"
            placeholder="Comment"
            onBlur={this.updateTimer}
            readOnly={logged}
            value={comment}
            onChange={this.handleCommentChange}
          />
        </div>
        <label className="timer-logged" title="Logged">
          <input
            name="timerLogged"
            type="checkbox"
            onChange={this.handleLoggedChange}
            checked={logged}
          />
          <span />
        </label>
      </form>
    );
  }
}

export default Tomato;
