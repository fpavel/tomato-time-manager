const RefreshButton = ({ onClick }) => (
  <button
    type="button"
    className="refresh-button"
    onClick={onClick}
  >Refresh</button>
);

export default RefreshButton;
