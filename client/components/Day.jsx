import { Component } from 'react';
import { formatDay } from '../utils/format-utils';
import TimersContainer from '../containers/TimersContainer';

class Day extends Component {
  shouldComponentUpdate({timers}) {
    const oldTimers = this.props.timers;
    let i = timers.length;
    if (i !== oldTimers.length) return true;
    while (i--) {
      if (timers[i] !== oldTimers[i]) return true;
    }
    return false;
  }
  render() {
    const { day, timers } = this.props;
    return (
      <div className="day">
        <h2>{ formatDay(day) } <span>{ timers.length }</span></h2>
        <TimersContainer timers={timers} />
      </div>
    );
  }
}

export default Day;
