import * as types from '../constants/action-types';
import { mergeTimers } from '../utils/timers-utils';

const timers = (state = [], action) => {
    switch (action.type) {
      case types.ADD_TIMERS:
        return mergeTimers(state, action.timers);
      case types.UPDATE_TIMER:
        return state.map(timer => timer.id === action.timer.id ? action.timer : timer);
      case types.RESET_TIMERS:
        return [];
      default:
        return state;
  }
};

export default timers;