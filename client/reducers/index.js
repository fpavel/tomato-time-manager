import { combineReducers } from 'redux';
import timers from './timers-reducers';
import filter from './filter-reducers';
import days from './days-reducers';
import activeTimer from './active-timer-reducer';

const tomatoApp = combineReducers({
  days,
  timers,
  filter,
  activeTimer,
});
export default tomatoApp;
