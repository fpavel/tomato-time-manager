import * as types from '../constants/action-types';

const filter = (state = '', action) => {
  switch (action.type) {
    case types.UPDATE_FILTER:
      return action.text;
    default:
      return state;
}
};

export default filter;