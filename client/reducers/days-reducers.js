import * as types from '../constants/action-types';
import { getUniqueDays } from '../utils/days-utils';

const days = (state = [], action) => {
    switch (action.type) {
      case types.ADD_TIMERS:
        return getUniqueDays(action.timers);
      case types.RESET_TIMERS:
        return [];
      default:
        return state;
  }
};

export default days;