import * as types from '../constants/action-types';

const activeTimer = (state = {}, action) => {
    switch (action.type) {
      case types.UPDATE_ACTIVE_TIMER:
        return action.timer;
      default:
        return state;
  }
};

export default activeTimer;