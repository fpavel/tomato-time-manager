import { PureComponent } from 'react';
import { connect } from 'react-redux';
import Tomato from '../components/Tomato';

class TimersContainer extends PureComponent {
  render() {
    const { timers, dispatch } = this.props;
    return (
      <div className="day-timers">
        { timers.map(tomato => <Tomato key={tomato.id} {...tomato} dispatch={dispatch} />) }
      </div>
    );
  }
}

export default connect()(TimersContainer);
