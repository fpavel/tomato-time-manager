import { connect } from 'react-redux';
import Day from '../components/Day';

const mapStateToProps = ({ days, filter, timers }, props) => ({
  days,
  timers: filter ? timers.filter(({comment}) => comment.toLowerCase().indexOf(filter.toLowerCase()) !== -1) : timers
});

const getTimersByDay = (day, timers) => timers.filter(timer => timer.day === day);

const DaysContainer = ({ days, timers, onUpdate }) =>
  days.map(day => {
    const dayTimers = getTimersByDay(day, timers);
    return dayTimers.length ? <Day key={day} day={day} timers={dayTimers} /> : null;
  });

export default connect(mapStateToProps)(DaysContainer);
