import { connect } from 'react-redux';
import { fetchTimers, fetchActiveTimer, updateFilter } from '../actions/timers-actions';
import Root from '../components/Root';

const mapStateToProps = (state, props) => ({
  activeTimer: state.activeTimer,
  filter: state.filter,
});

const RootContainer = (props) => <Root {...props} />;

export default connect(mapStateToProps, {
  fetchTimers,
  updateFilter,
  fetchActiveTimer,
})(RootContainer);