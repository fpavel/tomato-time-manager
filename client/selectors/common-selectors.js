import { createSelector } from 'reselect';
import { getUniqueDays } from '../utils/days-utils';

const getTimers = (state) => state.timers;

export const getDays = createSelector(
  getTimers,
  getUniqueDays
);
