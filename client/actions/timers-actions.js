import * as types from '../constants/action-types';
import { formatTimer } from '../utils/format-utils';


export const addTimers = (timers) => ({
  type: types.ADD_TIMERS,
  timers
});

export const fetchTimers = () => (dispatch) => {
  Pomodoro.getRecent().then(response => {
    dispatch(addTimers(response.map(formatTimer)));
  });
};

export const updateTimer = (timer) => ({
  type: types.UPDATE_TIMER,
  timer: formatTimer(timer)
});

export const updateFilter = (text) => ({
  type: types.UPDATE_FILTER,
  text
});

export const updateActiveTimer = (timer) => ({
  type: types.UPDATE_ACTIVE_TIMER,
  timer: formatTimer(timer)
});

export const timerPut = (timer) => (dispatch) => {
  Pomodoro.updateTimer(timer).then(timer => {
    dispatch(updateTimer(timer));
  });
};

export const fetchActiveTimer = () => (dispatch) => {
  Pomodoro.getActive().then(timer => {
    dispatch(updateActiveTimer(timer));
  });
};