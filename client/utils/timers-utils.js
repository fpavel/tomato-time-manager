import unionBy from 'lodash/unionBy';

export const mergeTimers = (oldTimers, newTimers) => {
  return unionBy(newTimers, oldTimers, 'id');
};