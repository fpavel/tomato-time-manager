import format from 'date-fns/format';
import { DAY_FROMAT } from '../constants/enviroment';

const today = format(new Date(), DAY_FROMAT);
const yesterday = format(new Date(Date.now() - 24 * 60 * 60 * 1000), DAY_FROMAT);

export const formatDay = (day) => today === day ? 'Today, ' + day : yesterday === day ? 'Yesterday, ' + day : day;

export const formatTimer = (timer) => {
  timer.date = new Date(timer.start * 1000);
  timer.day = format(timer.date, DAY_FROMAT);
  return timer;
};
