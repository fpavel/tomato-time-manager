export const getUniqueDays = (timers) => {
  const days = [];
  for(let i = 0, len = timers.length; i < len; i++) {
    if (days.indexOf(timers[i].day) === -1) days.push(timers[i].day);
  }
  return days;
};