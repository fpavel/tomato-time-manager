import { createStore, applyMiddleware, compose } from 'redux';
import thunk from 'redux-thunk';
import tomatoApp from './reducers';

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const configureStore = () => createStore(
  tomatoApp,
  composeEnhancers(
    applyMiddleware(thunk)
  )
);

export default configureStore;
