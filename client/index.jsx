import { render } from 'react-dom';
import { Provider } from 'react-redux';
import configureStore from './configureStore';
import RootContainer from './containers/RootContainer';


render(
  <Provider store={configureStore()}>
    <RootContainer />
  </Provider>,
  document.getElementById('main')
);
