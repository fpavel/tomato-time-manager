(function(Pomodoro) {
	const timerName = {};
	timerName[1] = 'Tomato';
	timerName[2] = 'Short break';
	timerName[3] = 'Long break';
	let timerInterval = null;

	const recentContainer = document.getElementById('recent-timers');
	const activeContainer = document.getElementById('active-timer');

	function renderTimerLogged(timer) {
		const input = (timer.logged)
			? `<input name="timerLogged" type="checkbox" checked="checked">`
			: `<input name="timerLogged" type="checkbox">`;
		return '<label class="timer-logged" title="Logged">' + input + `<span></span></label>`;
	}

	function renderTimer(timer) {
		const disabled = timer.logged ? 'readonly' : '';
		return `<div><form class="timer-wrapper" data-timer="${timer.id}" onchange="Pomodoro.updateTimer(this);return false;" onsubmit="Pomodoro.updateTimer(this);return false;">
							<div class="timer-time">${timer.formatStart} - ${timer.formatEnd}</div>
							<div class="timer-comment-wrapper">
								<input name="timerComment" ${disabled} placeholder="Comment" type="text" value="${timer.comment}">
							</div>
							${renderTimerLogged(timer)}
						</form></div>`;
	}

	function renderDay(day) {
		return `<div class="day">
							<h2 title="${day.date}">${day.day} (${day.timers.length})</h2>
							<div class="day-timers">${day.timers.map(renderTimer).join('')}</div>
						</div>`;
	}

	function renderRecent(days) {
		return `<div class="recent-timers">
							${days.map(renderDay).join('')}
						</div>`;
	}

	function renderStartButton(type) {
		const buttonName = timerName[type];
		const button = document.createElement('button');
		button.textContent = 'Start' + buttonName;
		// memory leak here
		button.addEventListener('click', e => {
			e.preventDefault();
			Pomodoro.startTimer().then(() => {
				startTimer();
			});
		});
		return button;
	}

	function renderTimeleft(timer) {
		let minutes = Math.trunc(timer.timeleft / 60);
		let seconds = timer.timeleft - minutes * 60;
		minutes = minutes < 10 ? '0' + minutes : minutes;
		seconds = seconds < 10 ? '0' + seconds : seconds;
		const div = document.createElement('div');
		div.className = 'timer-timeleft';
		div.textContent = `${timer.name} ${minutes}:${seconds}`;
		return div;
	}

	function renderActive(timer) {
		if (!activeContainer) return;
		activeContainer.innerHTML = '';
		activeContainer.appendChild(timer.next ? renderStartButton(timer.next) : renderTimeleft(timer));
	}

	function updateRecent() {
		if (!recentContainer) return;
		Pomodoro.getRecent().then(days => {
			recentContainer.innerHTML = renderRecent(days);
		});
	}

	function startTimer() {
		timerInterval = setInterval(() => {
			Pomodoro.getActive().then(active => {
				renderActive(active);
				if (!active.timeleft || active.timeleft < 0) {
					stopTimer();
					updateRecent();
				}
			});
		}, 1000);
	}

	function stopTimer() {
		if (timerInterval) clearInterval(timerInterval);
	}

	function updateTimer(form) {
		const id = form.getAttribute('data-timer');
		const container = form.parentElement;
		this.request('PUT', `/timers/${id}`, {
			comment: form.timerComment.value,
			logged: form.timerLogged.checked
		}).then(timer => {
			container.innerHTML = renderTimer(timer);
		});
	}

	// startTimer();
	updateRecent();

	Pomodoro.updateTimer = updateTimer;

}(Pomodoro));
