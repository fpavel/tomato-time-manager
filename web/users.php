<?php

require __DIR__ . '/../app.php';

app()->render('header');
app()->renderModule('menu');
app()->render('users', ['users' => app()->userService->getAllUsers()]);
app()->render('footer');