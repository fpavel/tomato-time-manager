<?php
require __DIR__ . '/../app.php';

if (app()->userService->getUserFromSession()->id) app()->redirect('/profile.php');

if (isset($_POST['username'], $_POST['email'], $_POST['password'], $_POST['password2'])) {
	app()->redirect(
		app()->userService->createUser($_POST) && app()->userService->authorize($_POST['username'], $_POST['password'])
		? '/index.php'
		: '/register.php?error=1'
	);
}

app()->render('header');
app()->render('register', ['error' => isset($_GET['error'])]);
app()->render('footer');