<?php

require __DIR__ . '/../app.php';
$user = app()->userService->getUserFromSession();
if (!$user->id) {
	app()->redirect('/login.php');
}
app()->render('header', ['apikey' => $user->apikey]);
app()->renderModule('menu');
app()->renderModule('front');
app()->render('footer');