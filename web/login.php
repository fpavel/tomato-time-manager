<?php
require __DIR__ . '/../app.php';

if (app()->userService->getUserFromSession()->id) app()->redirect('/profile.php');

if (isset($_POST['user'], $_POST['password'])) {
	$user = app()->userService->authorize($_POST['user'], $_POST['password']);
	app()->redirect($user ? '/index.php' : '/login.php?error=1');
}

app()->render('header');
app()->render('login', ['error' => isset($_GET['error'])]);
app()->render('footer');