<?php

require __DIR__ . '/../app.php';

$user = app()->userService->getUserFromSession();
if (!$user->id) {
	app()->redirect('/login.php');
}
$data = ['days' => app()->timerService->getCompletedByDay($user->id)];

app()->render('header', ['apikey' => $user->apikey]);
app()->renderModule('menu');
app()->render('report', $data);
app()->render('footer');